import authService from '@/services/auth/AuthService';
import { readFromStorage } from '@/store/persist-partial';

export const MUT_SET_TOKEN = 'MUT_SET_TOKEN';
export const MUT_SET_AUTH_FLAG = 'MUT_SET_AUTH_FLAG';
export const MUT_SET_USER_INFO = 'MUT_SET_USER_INFO';
export const MUT_SET_AUTHENTICATING_FLAG = 'MUT_SET_AUTHENTICATING_FLAG';
export const MUT_SET_LOGGING_OUT_FLAG = 'MUT_SET_LOGGING_OUT_FLAG';

export const ACT_START_LOGIN = 'ACT_START_LOGIN';
export const ACT_END_LOGIN = 'ACT_END_LOGIN';
export const ACT_START_LOGOUT = 'ACT_START_LOGOUT';
export const ACT_END_LOGOUT = 'ACT_END_LOGOUT';
export const ACT_READ_TOKEN = 'ACT_READ_TOKEN';
export const ACT_READ_PERSIST_DATA = 'ACT_READ_PERSIST_DATA';

const tokenInitial = {
  accessToken: null,
  idToken: null,
  expiresAt: null,
};

const userInfoInitial = {
  email: '',
  firstName: '',
  lastName: '',
  name: 'anonymous',
};

export default {
  state: {
    token: Object.assign({}, tokenInitial),
    userInfo: Object.assign({}, userInfoInitial),
    authenticated: false,
    authenticating: false,
    loggingOut: false,
  },
  getters: {
    authenticated: state => state.authenticated,
    authenticating: state => state.authenticating,
    loggingOut: state => state.loggingOut,
  },
  mutations: {
    [MUT_SET_TOKEN](state, { token }) {
      state.token = Object.assign({}, token);
    },
    [MUT_SET_USER_INFO](state, { userInfo }) {
      state.userInfo = Object.assign({}, userInfo);
    },
    [MUT_SET_AUTH_FLAG](state, payload) {
      state.authenticated = payload.authenticated;
    },
    [MUT_SET_AUTHENTICATING_FLAG](state, payload) {
      state.authenticating = payload.authenticating;
    },
    [MUT_SET_LOGGING_OUT_FLAG](state, payload) {
      state.loggingOut = payload.loggingOut;
    },
  },
  actions: {
    [ACT_START_LOGIN](context) {
      context.commit(MUT_SET_AUTHENTICATING_FLAG, { authenticating: true });
      authService.login();
    },
    [ACT_END_LOGIN](context) {
      authService.handleAuthentication()
      .then((data) => {
        context.commit(MUT_SET_AUTHENTICATING_FLAG, { authenticating: false });
        context.commit(MUT_SET_AUTH_FLAG, { authenticated: true });
        context.commit(MUT_SET_USER_INFO, { userInfo: data.userInfo });
        context.commit(MUT_SET_TOKEN, { token: data.token });
      })
      .catch((err) => {
        console.log('auth err', err);
        context.commit(MUT_SET_AUTHENTICATING_FLAG, { authenticating: false });
        context.commit(MUT_SET_USER_INFO, {});
      });
    },
    [ACT_START_LOGOUT](context) {
      authService.logout();
      context.commit(MUT_SET_LOGGING_OUT_FLAG, { loggingOut: true });
    },
    [ACT_END_LOGOUT](context) {
      context.commit(MUT_SET_LOGGING_OUT_FLAG, { loggingOut: false });
      context.commit(MUT_SET_AUTH_FLAG, { authenticated: false });
      context.commit(MUT_SET_TOKEN, { token: Object.assign({}, tokenInitial) });
      context.commit(MUT_SET_USER_INFO, { userInfo: Object.assign({}, userInfoInitial) });
    },
    [ACT_READ_PERSIST_DATA](context) {
      let token = readFromStorage('token');
      let userInfo = readFromStorage('userInfo');
      const authenticating = !!readFromStorage('authenticating');
      const loggingOut = !!readFromStorage('loggingOut');
      let authenticated = false;

      if (!token || !token.expiresAt || !authService.isAuthenticated(token.expiresAt)) {
        token = Object.assign({}, tokenInitial);
        userInfo = Object.assign({}, userInfoInitial);
      } else {
        authenticated = true;
      }
      context.commit(MUT_SET_TOKEN, { token });
      context.commit(MUT_SET_USER_INFO, { userInfo });
      context.commit(MUT_SET_AUTH_FLAG, { authenticated });
      context.commit(MUT_SET_AUTHENTICATING_FLAG, { authenticating });
      context.commit(MUT_SET_LOGGING_OUT_FLAG, { loggingOut });
    },
  },
};
