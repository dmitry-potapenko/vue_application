export const MUT_PUSH_MODAL = 'MUT_PUSH_MODAL';
export const MUT_POP_MODAL = 'MUT_POP_MODAL';

export const ACT_HIDE_MODAL = 'ACT_HIDE_MODAL';
export const ACT_SHOW_MODAL = 'ACT_SHOW_MODAL';

const module = {
  state: {
    stack: [],
  },
  mutations: {
    [MUT_PUSH_MODAL](state, payload) {
      state.stack.push(payload);
    },
    [MUT_POP_MODAL](state, payload) {
      state.stack.pop();
    },
  },
};
