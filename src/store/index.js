import Vuex from 'vuex';
import Vue from 'vue';

import auth from './modules/auth';
import addPersistStorage from './persist-partial';

import {
  SET_BADGES,
  SET_USER_NAME,
} from './user-mutations';

import {
  UPDATE_USER_NAME,
} from './user-actions';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  modules: {
    auth,
  },
  state: {
    user: {
      name: 'Vasiliy Terkin',
      badges: 0,
    },
  },
  mutations: {
    [SET_BADGES](state, payload) {
      state.user.badges = payload.count;
    },
    [SET_USER_NAME](state, payload) {
      state.user.name = payload.name;
    },
  },
  getters: {
    userName: state => state.user.name,
  },
  actions: {
    [UPDATE_USER_NAME]: (context, { timeout = 1e2 } = {}) => {
      setTimeout(() => {
        context.commit(SET_USER_NAME, { name: 'NewName' });
      }, timeout);
    },
  },
});

addPersistStorage(store);

export default store;
