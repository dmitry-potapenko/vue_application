import {
  MUT_SET_USER_INFO,
  MUT_SET_TOKEN,
  MUT_SET_AUTHENTICATING_FLAG,
  MUT_SET_LOGGING_OUT_FLAG } from '@/store/modules/auth';

const STORAGE_KEY = 'persist-vuex';

function updateStorage(key, value) {
  let store = localStorage.getItem(STORAGE_KEY);
  store = store !== null ? JSON.parse(store) : {};
  let newValue;
  if (value !== null && typeof value === 'object') {
    newValue = typeof store[key] === 'object' && store[key] !== null ? store[key] : {};
    newValue = Object.assign(newValue, value);
  } else {
    newValue = value;
  }
  store[key] = newValue;
  localStorage.setItem(STORAGE_KEY, JSON.stringify(store));
}

export function readFromStorage(key) {
  let store = localStorage.getItem(STORAGE_KEY);
  store = store !== null ? JSON.parse(store) : {};
  return store[key];
}

export default function (store) {
  store.subscribe((mutation) => {
    // mutation has type and payload, we can persist user
    switch (mutation.type) {
      case MUT_SET_USER_INFO:
        updateStorage('userInfo', mutation.payload.userInfo);
        break;
      case MUT_SET_TOKEN:
        updateStorage('token', mutation.payload.token);
        break;
      case MUT_SET_AUTHENTICATING_FLAG:
        updateStorage('authenticating', mutation.payload.authenticating);
        break;
      case MUT_SET_LOGGING_OUT_FLAG:
        updateStorage('loggingOut', mutation.payload.loggingOut);
        break;
    }
  });
}
