// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import 'normalize.css';
import '@/components/register-global-components';
import '@/assets/main.scss';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { ACT_READ_PERSIST_DATA } from '@/store/modules/auth';
import { mapActions } from 'vuex';
import App from './App';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

Vue.use(ElementUI);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  methods: {
    ...mapActions({
      loadPersistData: ACT_READ_PERSIST_DATA,
    }),
  },
  created() {
    return this.loadPersistData();
  },
});
