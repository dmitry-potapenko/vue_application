import auth0 from 'auth0-js';
import auth0Config from '@/config/auth0.json';

export class AuthService {
  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  auth0 = new auth0.WebAuth(auth0Config);

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) {
          return reject(err);
        }
        if (authResult && authResult.accessToken && authResult.idToken) {
          return resolve({
            token: {
              accessToken: authResult.accessToken,
              idToken: authResult.idToken,
              expiresAt: (authResult.expiresIn * 1000) + new Date().getTime(),
            },
            userInfo: {
              email: authResult.idTokenPayload.email,
              firstName: authResult.idTokenPayload.given_name,
              secondName: authResult.idTokenPayload.family_name,
              name: authResult.idTokenPayload.name,
            },
          });
        }
        return reject({ reason: 'No data!' });
      });
    });
  }

  login() {
    this.auth0.authorize(auth0Config);
  }

  logout() {
    this.auth0.logout({
      clientID: auth0Config.clientID,
      redirectTo: `${window.location.origin}/auth/logout`,
    });
  }

  isAuthenticated(expiresAt) {
    // Check whether the current time is past the
    // access token's expiry time
    return new Date().getTime() < +expiresAt;
  }
}

export default new AuthService();
