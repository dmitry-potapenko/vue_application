import Vue from 'vue';
import Router from 'vue-router';
import About from '@/routes/About/About';
import Home from '@/routes/Home/Home';
import NotFound from '@/routes/NotFound/NotFound';
import userRouting from '@/routes/User/router';
import authRouting from '@/routes/Auth/router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'main',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    ...userRouting,
    ...authRouting,
    {
      path: '*',
      component: NotFound,
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
