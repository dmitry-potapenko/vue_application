/* Main goal to register components globally */

import Vue from 'vue';
import Navbar from '@/components/Navbar';
import Main from '@/components/Main';
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import Logo from '@/components/Logo';
import AuthPanel from '@/components/AuthPanel';

/* Element-UI */
import { Button, Icon } from 'element-ui';

/* FontAwesome
* https://fontawesome.com/how-to-use/js-component-packages
* https://github.com/FortAwesome/vue-fontawesome
* */
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';

Vue.component('navbar', Navbar);
Vue.component('main-cmp', Main);
Vue.component('header-cmp', Header);
Vue.component('footer-cmp', Footer);
Vue.component('logo-cmp', Logo);
Vue.component('auth-panel-cmp', AuthPanel);
Vue.use(Button);
Vue.use(Icon);
Vue.component('font-awesome-icon', FontAwesomeIcon);
