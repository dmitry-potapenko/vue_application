import GoogleCallback from './GoogleCallback';
import LogoutCallback from './Logout';

export default [{
  path: '/auth/google',
  component: GoogleCallback,
}, {
  path: '/auth/logout',
  component: LogoutCallback,
}];
