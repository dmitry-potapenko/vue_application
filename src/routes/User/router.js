import User from './containers/User';
import UserHome from './containers/UserHome';
import UserProfile from './containers/UserProfile';


export default [{
  path: '/user/:id',
  component: User,
  children: [{
    path: '',
    component: UserHome,
  },
  {
    path: 'profile',
    component: UserProfile,
  },
  ],
}];
